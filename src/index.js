import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

// store
const initialState = {
  dummy: 0,
};

// reducer
const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "RANDOM_SCORE": {
      return {
        ...state,
        dummy: state.dummy,
      };
    }

    case "CHANGE_SCORE": {
      const generateScore = Math.floor(Math.random() * (100 + 100 + 1) - 100);
      return {
        ...state,
        dummy: generateScore,
      };
    }

    default: {
      return state;
    }
  }
};

export const store = createStore(rootReducer, applyMiddleware(thunk));

// subscribe
store.subscribe(() => {
  console.log("subscribe", store.getState());
});

// dispatch
store.dispatch({ type: "RANDOM_SCORE" });
store.dispatch({ type: "CHANGE_SCORE" });
console.log(store.getState());
