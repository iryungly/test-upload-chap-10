import React, { Component } from "react";
import { getDatabase, ref, onValue } from "firebase/database";
import firebaseInit from "/config/firebaseInit";

import "../assets/styles/UserProfileTable.module.css";

const db = getDatabase(firebaseInit);

export class PlayedGame extends Component {
  constructor() {
    super();
    this.state = {
      userData: [],
    };
  }

  componentDidMount() {
    const dbRef = ref(db, "user");
    onValue(dbRef, (snapshot) => {
      let record = [];
      snapshot.forEach((childSnapshot) => {
        let keyName = childSnapshot.key;
        let data = childSnapshot.val();
        record.push({ key: keyName, data: data });
      });

      this.setState({ userData: record });
    });
  }

  render() {
    return (
      <div>
        <h3
          style={{
            textAlign: "center",background: "#000221"
          }}
        >
          List User Played Game
        </h3>
        <div className="table-wrapper" style={{
            textAlign: "center",background: "black"
          }}>
          <table
            className="fl-table"
            style={{
              marginLeft: "auto",
              marginRight: "auto",
              border: "1px solid white",
              marginBottom: "150px",
            }}
          >
            <thead>
              <tr>
                <th style={{ padding: "7px" }}>#</th>
                <th style={{ padding: "7px" }}>username</th>
                <th style={{ padding: "7px" }}>status</th>
              </tr>
            </thead>
            <tbody>
              {this.state.userData.map((row, index) => {
                return (
                  <>
                    <tr >
                      <td style={{ padding: "7px" }}>{index}</td>
                      <td style={{ padding: "7px" }}>{row.data.username}</td>
                      <td style={{ padding: "7px" }}>{row.data.status}</td>
                    </tr>
                  </>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
export default PlayedGame;
