import React, { useState, useEffect } from 'react'
import { Navbar, Container, Nav, NavLink } from "react-bootstrap";
import { useCookies } from "react-cookie";
import jwt from "jwt-decode";
import Image from 'next/image';

const NavBar = () => {
  const [cookies, setCookies, deleteCookies] = useCookies()

  const [registerBtnDisplay, setRegisterBtnDisplay] = useState("flex")
  const [logInBtnDisplay, setLogInBtnDisplay] = useState("block")
  const [profileBtnDisplay, setProfileBtnDisplay] = useState("flex")
  const [Username, setUsername] = useState("username")
  const [LogInOut, setLogInOut] = useState("Login")
  const [LogInOutStateState, setLogInOutStateState] = useState("blm_login")
  const [ProfileImage, setProfileImage] = useState("blankpp.webp")

  function loginout() {
    if (LogInOutStateState === "blm_login") {
      window.location.href = "/login"
    } else {
      deleteCookies("tgtoken")
      deleteCookies("googlelogin")
      window.location.href = "/"
    }

  }
  return (
    <Navbar collapseOnSelect expand="lg" variant="dark" id="navigations">
      <Container fluid>
        <Navbar.Brand href="/">
          <Image src='/game-logo.png' alt='' 
          width={10}
          height={45}/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="nav-left">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/gamelist">Games</Nav.Link>
            <Nav.Link href="/about">About Us</Nav.Link>
            <Nav.Link href="/blog">Blog</Nav.Link>
            <Nav.Link href="/contact">Contact</Nav.Link>
          </Nav>
          <Nav className="nav-right">
            <Nav.Link href="/profile" style={{ display: profileBtnDisplay, alignItems: "center", marginRight:"2px" }}>
              {Username}</Nav.Link>
            <Nav.Link href="/register" style={{ display: registerBtnDisplay }}><button>Register</button></Nav.Link>
            <Nav.Link style={{ display: logInBtnDisplay}} 
            onClick={loginout}><button>{LogInOut}</button></Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default NavBar