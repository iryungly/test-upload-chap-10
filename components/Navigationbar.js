import style from '../assets/styles/NavigationBar.module.css';
import Link from 'next/link';
import MyScore from './MyScore';

export default function NavigationBar() {
  return (
    <>
      <header className={style.header}>
        <div className={style.container}>
          <nav className={style.nav}>
            <ul className={style.ul}>
              {/* NavBar Link */}
              <li className={style.li}>
                <Link className={style.a} href="/">
                  LandingPage
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/gamelist">
                  GameList
                </Link>
              </li>

               <li className={style.li}>
                <Link className={style.a} href="/game">
                  Game
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/gamedetail">
                  GameDetail
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/story">
                  Story
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/userprofile">
                  UserProfile
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/updateuser">
                  ProfileUpdate
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/leaderboard">
                  Leaderboard
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/newgame">
                  NewGame
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/currentinfo">
                  CurrentInfo
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/dummy_games">
                  DummyGames
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/">
                  Logout
                </Link>
              </li>

            </ul>
            <MyScore />
          </nav>
        </div>
      </header>
    </>
  );
}
