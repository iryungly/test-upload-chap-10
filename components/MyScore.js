import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { useEffect, useState } from 'react';
import { getDatabase, ref, onValue } from 'firebase/database';

function MyScore() {
  const auth = getAuth();
  const db = getDatabase();
  const [email, setEmail] = useState('');
  const [score, setScore] = useState('');
  function getScore() {
    const userId = auth.currentUser.uid;
    return onValue(
      ref(db, '/users/' + userId),
      (snapshot) => {
        console.log(snapshot.val().score, 'snapshot');
        const scoreNilai = (snapshot.val() && snapshot.val().score) || '0';
        setScore(scoreNilai);
      },
      {
        onlyOnce: true,
      }
    );
  }

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        console.log(user, 'USER GOOGLE');
        setEmail(user.email);
        getScore();
      } else {
      }
    });
  },);

  return (
    <div>
      {email != null && email != '' && email !== undefined ? (
        <div className="row" style={{ background: 'blue' }}>
          <div className="col text-center">
            Hi {email}, Skor Anda : {score}
          </div>
        </div>
      ) : (
        ''
      )}
    </div>
  );
}

export default MyScore;
