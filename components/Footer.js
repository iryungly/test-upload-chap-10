import style from "../assets/styles/Footer.module.css";
import Link from "next/link";
import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  LineIcon,
  LineShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
} from "react-share";

export default function Footer() {
  const url = "https://gitlab.com/faufauzi94/binar-challenge-chapter-10-fsw-25";

  const title = "Binar Academy Challenge Chapter 10";
  return (
    <div className={style.body}>
      <footer className={style.footer}>
        <div>
          <h3>Binar Academy Challenge Chapter 10 - FSW 25</h3>
          <p>
            We are from Group 1 have completed the Chapter 10 Challenge from
            Binar Academy <br /> We started the Challenge since December 07,
            2022 and finished at December 19, 2022
          </p>
        </div>

        <div>
          <button className={style.button_28} role="button">
            <Link href="/about">About</Link>
          </button>
          <button className={style.button_28} role="button">
            <Link href="/feature">Feature</Link>
          </button>
          <button className={style.button_28} role="button">
            <svg
              width="15"
              height="15"
              viewBox="0 0 256 236"
              xmlns="http://www.w3.org/2000/svg"
              preserveAspectRatio="xMinYMin meet"
            >
              <path
                d="M128.075 236.075l47.104-144.97H80.97l47.104 144.97z"
                fill="#E24329"
              />
              <path
                d="M128.075 236.074L80.97 91.104H14.956l113.119 144.97z"
                fill="#FC6D26"
              />
              <path
                d="M14.956 91.104L.642 135.16a9.752 9.752 0 0 0 3.542 10.903l123.891 90.012-113.12-144.97z"
                fill="#FCA326"
              />
              <path
                d="M14.956 91.105H80.97L52.601 3.79c-1.46-4.493-7.816-4.492-9.275 0l-28.37 87.315z"
                fill="#E24329"
              />
              <path
                d="M128.075 236.074l47.104-144.97h66.015l-113.12 144.97z"
                fill="#FC6D26"
              />
              <path
                d="M241.194 91.104l14.314 44.056a9.752 9.752 0 0 1-3.543 10.903l-123.89 90.012 113.119-144.97z"
                fill="#FCA326"
              />
              <path
                d="M241.194 91.105h-66.015l28.37-87.315c1.46-4.493 7.816-4.492 9.275 0l28.37 87.315z"
                fill="#E24329"
              />
            </svg>
            &nbsp;
            <a
              href="https://gitlab.com/faufauzi94/binar-challenge-chapter-10-fsw-25"
              target="noreferrer"
            >
              Repository
            </a>
          </button>
        </div>

        <hr className={style.horizline} />

        <div className={style.share}>
          <p>You can also share this project on your social media :</p>
          <div className="mt-2 text-center">
            <FacebookShareButton url={url} quote={title}>
              <FacebookIcon size={35} className={style.icon} />
            </FacebookShareButton>

            <EmailShareButton url={url} quote={title}>
              <EmailIcon size={35} className={style.icon} />
            </EmailShareButton>

            <LineShareButton url={url} quote={title}>
              <LineIcon size={35} className={style.icon} />
            </LineShareButton>

            <LinkedinShareButton url={url} quote={title}>
              <LinkedinIcon size={35} className={style.icon} />
            </LinkedinShareButton>

            <TelegramShareButton url={url} quote={title}>
              <TelegramIcon size={35} className={style.icon} />
            </TelegramShareButton>

            <TwitterShareButton url={url} quote={title}>
              <TwitterIcon size={35} className={style.icon} />
            </TwitterShareButton>

            <WhatsappShareButton url={url} quote={title}>
              <WhatsappIcon size={35} className={style.icon} />
            </WhatsappShareButton>
          </div>
        </div>
      </footer>
    </div>
  );
}
