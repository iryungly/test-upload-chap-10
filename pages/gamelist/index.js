import Navigationbar from "../../components/Navigationbar";
import { getDatabase, ref, onValue } from "firebase/database";
import firebaseInit from "config/firebaseInit";
import { useEffect, useState, useRef } from "react";
import React from "react";
import Link from "next/link";
import Image from "next/image";

import PlayedGame from "/components/playedgame";

import NavigationBar from "../../components/Navigationbar";
import Footer from "../../components/Footer";

import style from "../../assets/styles/Gamelist.module.css";

const Gamelist = () => {
  const [user, setUser] = useState([]);

  const url = "http://localhost:3000";
  const username = useRef("");
  const [isPlayed, setIsPlayed] = useState(false);
  useEffect(() => {
    const db = getDatabase(firebaseInit);
    const dbRef = ref(db, "user");
    onValue(dbRef, (snapshot) => {
      setUser(snapshot.val());
    });
  }, []);

  return (
    <div>
      <NavigationBar />

      <div className={style.body}>
        <h1 className={style.h1}>GameList</h1>
        <div className={style.flex_container_cards}>
          {/* Game 1 */}
          <div className={style.card}>
            <div className={style.card_img_shadow}>
              <Image
                src={"/image/game1.jpeg"}
                width={200}
                height={200}
                className={style.img}
                alt=""
              />
            </div>
            <div className={style.card_content}>
              <h4 className={style.h4}>
                Fireboy <br /> & <br /> Watergirl
              </h4>
              <h4 className="text-center mt-3">
                <br />
              </h4>
              <div className={style.button_shadow}>
                <div
                  className={style.card_button}
                  onClick={() => {
                    alert("Game is not available!");
                  }}
                >
                  View More
                </div>
              </div>
            </div>
          </div>

          {/* Game 2 */}
          <div className={style.card}>
            <div className={style.card_img_shadow}>
              <Image
                src={"/image/logo.png"}
                width={200}
                height={200}
                className={style.img}
                alt=""
              />
            </div>
            <div className={style.card_content}>
              <h4 className={style.h4}>
                Rock <br /> Paper <br /> Scissors
              </h4>
              <h4 className="text-center mt-3">
                {isPlayed ? "Haven't Played" : "Played Before"} <br />
              </h4>
              <div className={style.button_shadow}>
                <div className={style.card_button}>
                  <Link href="/gamedetail" className={style.view}>
                    View More
                  </Link>
                </div>
              </div>
            </div>
          </div>

          {/* Game 3 */}
          <div className={style.card}>
            <div className={style.card_img_shadow}>
              <Image
                src={"/image/checkers.jpg"}
                width={200}
                height={200}
                className={style.img}
                alt=""
              />
            </div>
            <div className={style.card_content}>
              <h4 className={style.h4}>
                Casual <br /> Checkers <br /> <br />
              </h4>
              <h4 className="text-center mt-3">
                <br />
              </h4>
              <div className={style.button_shadow}>
                <div
                  className={style.card_button}
                  onClick={() => {
                    alert("Game is not available!");
                  }}
                >
                  View More
                </div>
              </div>
            </div>
          </div>
        </div>

        <button className={style.button_64} role="button">
          <span className={style.span}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              style={{ verticalAlign: "-0.125em" }}
              width="1em"
              height="1em"
              preserveAspectRatio="xMidYMid meet"
              viewBox="0 0 24 24"
              className={style.svg}
            >
              <g transform="translate(24 0) scale(-1 1)">
                <path
                  fill="white"
                  d="M14.94 19.5L12 17.77L9.06 19.5l.78-3.34l-2.59-2.24l3.41-.29L12 10.5l1.34 3.13l3.41.29l-2.59 2.24M20 2H4v2l4.86 3.64a8 8 0 1 0 6.28 0L20 4m-2 11a6 6 0 1 1-7.18-5.88a5.86 5.86 0 0 1 2.36 0A6 6 0 0 1 18 15m-5.37-8h-1.26l-4-3h9.34Z"
                />
              </g>
            </svg>
            <Link href="/badge" className={style.a}>
              Badge Achievement
            </Link>
          </span>
        </button>
      </div>
      <PlayedGame />
      <Footer />
    </div>
  );
};

export default Gamelist;
