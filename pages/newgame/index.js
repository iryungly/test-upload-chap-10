import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import NavBar from '/components/Navigationbar';

const newgame = () => {
  return (
    <div>
      <NavBar />
      <section style={{ background: 'purple' }}>
        <div className="container d-flex justify-content-center align-items-center vw-100 vh-100">
          <Card className="text-center" style={{ color: 'purple', width: '400px', padding: '20px' }}>
            <Card.Img variant="top" src="newgame.jpg" style={{ width: '300px', margin: '0 auto' }} />
            <Card.Body>
              <Card.Title>NEW GAME</Card.Title>
              <Card.Text>Ini adalah game baru yang akan datang</Card.Text>
              <Button variant="danger" disabled>
                New Game
              </Button>
            </Card.Body>
          </Card>
        </div>
      </section>
    </div>
  );
};

export default newgame;
