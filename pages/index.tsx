import Head from 'next/head'
import NavBar from '../components/Navbar'
import Link from 'next/link'
import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'

export default function Home() {
  return (
    <>
      <Head>
        <title>GAMES ~ HOME</title>
      </Head>
      <NavBar />
      <div className='landing-page'>
        <div className='container' id='section1'>
          <div className='main-text'>
            <div className='row best-game'>
              <h1>WONDERFUL <span>GAME</span></h1>
            </div>
            <h1><span>PLAYING</span> TODAY</h1>
            <p>We Create good games experience to make you who the only one in this world who feels the sensation of playing a traditional game</p>
            <div className='button-main'>
              <Link href='/login'>
                <button className='play-btn'>Play Now ↗</button>
              </Link>
              <Link href='/about'>
                <button className='explore-btn'>Explore</button>
              </Link>
            </div>
          </div>
        </div>
      </div>  
    </>
  )
}
