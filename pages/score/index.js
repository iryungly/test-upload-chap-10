import { useState } from 'react';
import Swal from 'sweetalert2';
import { auth, firebaseInit } from '../../config/firebaseInit';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { useRouter } from 'next/router';
import NavBar from '/components/Navigationbar';
import { getDatabase, ref, set, push } from 'firebase/database';
import { useEffect, useRef } from 'react';

const Score = () => {
  const [score, setscore] = useState('');
  const router = useRouter();

  return (
    <div className="row">
      <div className="form-group col-md-12">
        <label htmlFor="name">Skor</label>
        <input type="score" readOnly className="form-control" id="score" required placeholder="" value={score} />
      </div>
    </div>
  );
};

export default Score;
