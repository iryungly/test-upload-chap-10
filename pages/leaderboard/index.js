import { getDatabase, ref, onValue } from "firebase/database";
import firebaseInit from "config/firebaseInit";
import { useEffect, useState, useRef } from "react";
import Image from "next/image";
import NavigationBar from "../../components/Navigationbar";
import Footer from "../../components/Footer";

const Leaderboard = () => {
  const [userstory, setUser] = useState([]);
  useEffect(() => {
    const db = getDatabase(firebaseInit);
    const dbRef = ref(db, "userstory");
    onValue(dbRef, (snapshot) => {
      setUser(snapshot.val());
    });
  }, []);

  const handleClick = (e) => {
    console.log(e.target);
  };

  return (
    <>
      <NavigationBar />
      <div className="board">
        <h1 className="leaderboard">Leaderboard</h1>

        <div className="duration">
          <button onClick={handleClick} data-id="7">
            7 Days
          </button>
          <button onClick={handleClick} data-id="30">
            30 Days
          </button>
          <button onClick={handleClick} data-id="0">
            All-Time
          </button>
        </div>
        <div id="profile">
          <div class="card">
            <div className="flex">
              <div className="flex">
                <div className="info">
                  <table class="table">
                    <tr>
                      <th>USERNAME</th>
                    </tr>
                    <br />
                  </table>
                  {userstory &&
                    userstory.map((userstory, key) => {
                      return (
                        <table class="table"key="">
                          <tr key={key}>
                            <td> {userstory.username}</td>
                          </tr>
                          <br />
                        </table>
                      );
                    })}
                </div>
              </div>
              <div className="info">
                <table class="table">
                  <tr>
                    <th>SCORE</th>
                  </tr>
                  <br />
                </table>
                {userstory &&
                  userstory.map((userstory, key) => {
                    return (
                      <table class="table"key="">
                        <tr key={key}>
                          <td> {userstory.score}</td>
                        </tr>
                        <br />
                      </table>
                    );
                  })}
              </div>
              <div className="info">
                <table class="table">
                  <tr>
                    <th>LEVEL</th>
                  </tr>
                  <br />
                </table>
                <table class="table">
                  <th>
                    <tr class="level">
                      <Image
                        src={require("/public/image/level9.png")}
                        width={70}
                        height={70}
                        alt="level9"
                      />
                    </tr>
                    <tr class="level">
                      <Image
                        src={require("/public/image/level5.png")}
                        width={70}
                        height={70}
                        alt="level5"
                      />
                    </tr>
                    <tr class="level">
                      <Image
                        src={require("/public/image/level5.png")}
                        width={70}
                        height={70}
                        alt="level5"
                      />
                    </tr>
                    <tr class="level">
                      <Image
                        src={require("/public/image/level9.png")}
                        width={70}
                        height={70}
                        alt="level9"
                      />
                    </tr>
                    <tr class="level">
                      <Image
                        src={require("/public/image/level10.png")}
                        width={70}
                        height={70}
                        alt="level10"
                      />
                    </tr>
                    <tr class="level">
                      <Image
                        src={require("/public/image/level2.png")}
                        width={70}
                        height={70}
                        alt="level2"
                      />
                    </tr>
                    <tr class="level">
                      <Image
                        src={require("/public/image/level2.png")}
                        width={70}
                        height={70}
                        alt="level2"
                      />
                    </tr>
                  </th>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Leaderboard;
