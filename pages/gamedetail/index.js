import Link from 'next/link'
import Image from 'next/image';
import { useState } from "react";
import NavBar from '/components/Navigationbar'

function Play(props) {
  const { status } = props;

  if (status === true) {
    return (
    <>
    
    <br/>
    
    <h1 className="text-center mt-3" style={{ color: `purple` }}><Link href="/playgame"> <Image src={require("/public/image/logo.png")} padding={10} width={50} height={50}alt="" />  Rock-Paper-Scissor   </Link></h1>
    </>
  );

  }
  return <> Game Detail</>;
}

function Gamedetail() {
  const [playGame, setPlayGame] = useState(false);
  return (
    <>
      <NavBar/>
      <ul>
        
          <h5 className="text-center mt-3">.</h5>
          <h1 className="text-center mt-3" style={{ color: `purple` }}> Game Play</h1>
          <h2 className="text-center mt-3" style={{ color: `purple` }}>Choose your game:</h2>
          <h1 className="text-center mt-3" style={{ color: `purple`}}><button onClick={() => setPlayGame(true)} >
            Play Game | Click to Play
          </button></h1>
      </ul>
      <Play status={playGame} />
    </>
  );
}
export default Gamedetail


